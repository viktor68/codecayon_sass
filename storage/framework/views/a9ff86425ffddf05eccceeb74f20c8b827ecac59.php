<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">

                <div class="col-md-1 col-xs-3">
                    <?php echo ($row->image) ? '<img src="'.asset('user-uploads/avatar/'.$row->image).'" alt="user" class="img-circle" width="40">' : '<img src="'.asset('default-profile-2.png').'" alt="user" class="img-circle" width="40">'; ?>

                </div>
                <div class="col-md-11 col-xs-9">
                    <?php echo e(ucwords($row->name)); ?> <br>
                    <span class="font-light text-muted"><?php echo e(ucfirst($row->job_title)); ?></span>
                </div>
                <div class="clearfix"></div>

            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row">
                        <?php echo Form::open(['id'=>'attendance-container-'.$row->id,'class'=>'ajax-form','method'=>'POST']); ?>

                        <div class="form-body ">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" ><?php echo app('translator')->getFromJson('modules.attendance.late'); ?></label>
                                        <?php if(count($row->attendance) > 0): ?>
                                            <?php if($row->attendance[0]->late == "yes"): ?> <span class="label label-success"><i class="fa fa-check"></i> <?php echo app('translator')->getFromJson('app.yes'); ?></span>
                                            <?php else: ?>
                                                <span class="label label-danger"><i class="fa fa-times"></i> <?php echo app('translator')->getFromJson('app.no'); ?></span>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            --
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" ><?php echo app('translator')->getFromJson('modules.attendance.halfDay'); ?></label>
                                        <?php if(count($row->attendance) > 0): ?>
                                            <?php if($row->attendance[0]->half_day == "yes"): ?> <span class="label label-success"><i class="fa fa-check"></i> <?php echo app('translator')->getFromJson('app.yes'); ?></span>
                                            <?php else: ?>
                                                <span class="label label-danger"><i class="fa fa-times"></i> <?php echo app('translator')->getFromJson('app.no'); ?></span>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            --
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?></th>
                                            <th><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?></th>
                                            <th><?php echo app('translator')->getFromJson('app.others'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__empty_1 = true; $__currentLoopData = $row->attendance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $attendance): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <tr>
                                                <td width="30%" class="al-center bt-border">
                                                    <?php echo e($attendance->clock_in_time->timezone($global->timezone)->format($global->time_format)); ?>

                                                </td>
                                                <td width="30%" class="al-center bt-border">
                                                    <?php if(!is_null($attendance->clock_out_time)): ?> <?php echo e($attendance->clock_out_time->timezone($global->timezone)->format($global->time_format)); ?> <?php else: ?> - <?php endif; ?>
                                                </td>
                                                <td class="bt-border" style="padding-bottom: 5px;">
                                                    <strong><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?> IP: </strong> <?php echo e($attendance->clock_in_ip); ?><br>
                                                    <strong><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?> IP: </strong> <?php echo e($attendance->clock_out_ip); ?><br>
                                                    <strong><?php echo app('translator')->getFromJson('modules.attendance.working_from'); ?>: </strong> <?php echo e($attendance->working_from); ?><br>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <tr>
                                                <td colspan="3"><?php echo app('translator')->getFromJson('messages.noAttendanceDetail'); ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>