<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->getFromJson('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.settings.index')); ?>"><?php echo app('translator')->getFromJson('app.menu.settings'); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"><?php echo app('translator')->getFromJson('app.menu.ticketChannel'); ?></div>

                <div class="vtabs customvtab m-t-10">

                    <?php echo $__env->make('sections.ticket_setting_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="white-box">
                                        <h3><?php echo app('translator')->getFromJson('app.addNew'); ?> <?php echo app('translator')->getFromJson('app.menu.ticketChannel'); ?></h3>

                                        <?php echo Form::open(['id'=>'createChannel','class'=>'ajax-form','method'=>'POST']); ?>


                                        <div class="form-body">

                                            <div class="form-group">
                                                <label><?php echo app('translator')->getFromJson('modules.tickets.channelName'); ?></label>
                                                <input type="text" name="channel_name" id="channel_name" class="form-control">
                                            </div>

                                            <div class="form-actions">
                                                <button type="submit" id="save-channel" class="btn btn-success"><i
                                                            class="fa fa-check"></i> <?php echo app('translator')->getFromJson('app.save'); ?>
                                                </button>
                                            </div>
                                        </div>

                                        <?php echo Form::close(); ?>


                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="white-box">
                                        <h3><?php echo app('translator')->getFromJson('app.menu.ticketChannel'); ?></h3>


                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th><?php echo app('translator')->getFromJson('app.name'); ?></th>
                                                    <th><?php echo app('translator')->getFromJson('app.action'); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $__empty_1 = true; $__currentLoopData = $channels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$channel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                    <tr>
                                                        <td><?php echo e(($key+1)); ?></td>
                                                        <td><?php echo e(ucwords($channel->channel_name)); ?></td>
                                                        <td>
                                                            <a href="javascript:;" data-channel-id="<?php echo e($channel->id); ?>"
                                                               class="btn btn-sm btn-info btn-rounded btn-outline edit-channel"><i
                                                                        class="fa fa-edit"></i> <?php echo app('translator')->getFromJson('app.edit'); ?></a>
                                                            <a href="javascript:;" data-channel-id="<?php echo e($channel->id); ?>"
                                                               class="btn btn-sm btn-danger btn-rounded btn-outline delete-channel"><i
                                                                        class="fa fa-times"></i> <?php echo app('translator')->getFromJson('app.remove'); ?></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo app('translator')->getFromJson('messages.noTicketChannelAdded'); ?>
                                                        </td>
                                                    </tr>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
    <!-- .row -->


    
    <div class="modal fade bs-modal-md in" id="ticketChannelModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script type="text/javascript">


    //    save project members
    $('#save-channel').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.ticketChannels.store')); ?>',
            container: '#createChannel',
            type: "POST",
            data: $('#createChannel').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    window.location.reload();
                }
            }
        })
    });


    $('body').on('click', '.delete-channel', function () {
        var id = $(this).data('channel-id');
        swal({
            title: "Are you sure?",
            text: "This will remove the channel type from the list.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "<?php echo e(route('admin.ticketChannels.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });


    $('.edit-channel').click(function () {
        var typeId = $(this).data('channel-id');
        var url = '<?php echo e(route("admin.ticketChannels.edit", ":id")); ?>';
        url = url.replace(':id', typeId);

        $('#modelHeading').html("<?php echo e(__('app.edit')." ".__('app.menu.ticketChannel')); ?>");
        $.ajaxModal('#ticketChannelModal', url);
    })


</script>


<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>