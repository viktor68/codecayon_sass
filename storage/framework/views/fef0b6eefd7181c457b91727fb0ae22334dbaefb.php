<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->getFromJson('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.tickets.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->getFromJson('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo Form::open(['id'=>'storeTicket','class'=>'ajax-form','method'=>'POST']); ?>

    <div class="form-body">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-right"><?php echo app('translator')->getFromJson('modules.tickets.ticket'); ?> # <?php echo e((is_null($lastTicket)) ? "1" : ($lastTicket->id+1)); ?></div>

                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.tickets.ticketSubject'); ?> <span class="text-danger">*</span></label>
                                        <input type="text" id="subject" name="subject" class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.tickets.ticketDescription'); ?> <span class="text-danger">*</span></label></label>
                                        <textarea class="textarea_editor form-control" rows="10" name="description"
                                                  id="description"></textarea>
                                    </div>
                                </div>
                                <!--/span-->

                                <?php echo Form::hidden('status', 'open', ['id' => 'status']); ?>


                            </div>
                            <!--/row-->

                        </div>
                    </div>

                    <div class="panel-footer text-right">
                        <div class="btn-group dropup m-r-10">
                            <button aria-expanded="true" data-toggle="dropdown"
                                    class="btn btn-info btn-outline dropdown-toggle waves-effect waves-light"
                                    type="button"><i class="fa fa-bolt"></i> <?php echo app('translator')->getFromJson('modules.tickets.applyTemplate'); ?>
                                <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <?php $__empty_1 = true; $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <li><a href="javascript:;" data-template-id="<?php echo e($template->id); ?>" class="apply-template"><?php echo e(ucfirst($template->reply_heading)); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <li><?php echo app('translator')->getFromJson('messages.noTemplateFound'); ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="btn-group dropup">
                            <button aria-expanded="true" data-toggle="dropdown"
                                    class="btn btn-success dropdown-toggle waves-effect waves-light"
                                    type="button"><?php echo app('translator')->getFromJson('app.submit'); ?> <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="javascript:;" class="submit-ticket" data-status="open"><?php echo app('translator')->getFromJson('app.submit'); ?> <?php echo app('translator')->getFromJson('app.open'); ?>
                                        <span style="width: 15px; height: 15px;"
                                              class="btn btn-danger btn-small btn-circle">&nbsp;</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="submit-ticket" data-status="pending"><?php echo app('translator')->getFromJson('app.submit'); ?> <?php echo app('translator')->getFromJson('app.pending'); ?>
                                        <span style="width: 15px; height: 15px;"
                                              class="btn btn-warning btn-small btn-circle">&nbsp;</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="submit-ticket" data-status="resolved"><?php echo app('translator')->getFromJson('app.submit'); ?> <?php echo app('translator')->getFromJson('app.resolved'); ?>
                                        <span style="width: 15px; height: 15px;"
                                              class="btn btn-info btn-small btn-circle">&nbsp;</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="submit-ticket" data-status="closed"><?php echo app('translator')->getFromJson('app.submit'); ?> <?php echo app('translator')->getFromJson('app.close'); ?>
                                        <span style="width: 15px; height: 15px;"
                                              class="btn btn-success btn-small btn-circle">&nbsp;</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-4">
                <div class="panel panel-default">

                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.tickets.requesterName'); ?></label>
                                        <select  name="user_id" id="user_id" class="select2 form-control" data-style="form-control" >
                                            <option value=""><?php echo app('translator')->getFromJson('app.select'); ?> <?php echo app('translator')->getFromJson('modules.tickets.requesterName'); ?></option>
                                            <?php $__currentLoopData = $requesters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $requester): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($requester->id); ?>"><?php echo e(ucwords($requester->name).' ['.$requester->email.']'); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.tickets.agent'); ?></label>
                                        <select  name="agent_id" id="agent_id" class="select2 form-control" data-style="form-control" >
                                            <option value="">Agent not assigned</option>
                                            <?php $__empty_1 = true; $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <?php if(count($group->enabled_agents) > 0): ?>
                                                    <optgroup label="<?php echo e(ucwords($group->group_name)); ?>">
                                                        <?php $__currentLoopData = $group->enabled_agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($agent->user->id); ?>"><?php echo e(ucwords($agent->user->name).' ['.$agent->user->email.']'); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </optgroup>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <option value=""><?php echo app('translator')->getFromJson('messages.noGroupAdded'); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.invoices.type'); ?> <a class="btn btn-xs btn-info btn-outline" href="javascript:;" id="add-type"><i class="fa fa-plus"></i> <?php echo app('translator')->getFromJson('modules.tickets.addType'); ?></a></label>
                                        <select class="form-control selectpicker add-type" name="type_id" id="type_id" data-style="form-control">
                                            <?php $__empty_1 = true; $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <option value="<?php echo e($type->id); ?>"><?php echo e(ucwords($type->type)); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <option value=""><?php echo app('translator')->getFromJson('messages.noTicketTypeAdded'); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.tasks.priority'); ?> <span class="text-danger">*</span></label>
                                        <select class="form-control selectpicker" name="priority" id="priority" data-style="form-control">
                                            <option value="low"><?php echo app('translator')->getFromJson('app.low'); ?></option>
                                            <option value="medium"><?php echo app('translator')->getFromJson('app.medium'); ?></option>
                                            <option value="high"><?php echo app('translator')->getFromJson('app.high'); ?></option>
                                            <option value="urgent"><?php echo app('translator')->getFromJson('app.urgent'); ?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.tickets.channelName'); ?> <a class="btn btn-xs btn-info btn-outline" href="javascript:;" id="add-channel"><i class="fa fa-plus"></i> <?php echo app('translator')->getFromJson('modules.tickets.addChannel'); ?></a></label>
                                        <select class="form-control selectpicker" name="channel_id" id="channel_id" data-style="form-control">
                                            <?php $__empty_1 = true; $__currentLoopData = $channels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $channel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <option value="<?php echo e($channel->id); ?>"><?php echo e(ucwords($channel->channel_name)); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <option value=""><?php echo app('translator')->getFromJson('messages.noTicketChannelAdded'); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.tickets.tags'); ?></label>
                                        <select multiple data-role="tagsinput" name="tags[]" id="tags">

                                        </select>
                                    </div>
                                </div>

                                <!--/span-->

                            </div>
                            <!--/row-->

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- .row -->
    </div>
    <?php echo Form::close(); ?>


    
    <div class="modal fade bs-modal-md in" id="ticketModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>


<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/html5-editor/wysihtml5-0.3.0.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script>
    $('.textarea_editor').wysihtml5();

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    $('.apply-template').click(function () {
        var templateId = $(this).data('template-id');
        var token = '<?php echo e(csrf_token()); ?>';

        $.easyAjax({
            url: '<?php echo e(route('admin.replyTemplates.fetchTemplate')); ?>',
            type: "POST",
            data: { _token: token, templateId: templateId },
            success: function (response) {
                if (response.status == "success") {
                    var editorObj = $("#description").data('wysihtml5');
                    var editor = editorObj.editor;
                    editor.setValue(response.replyText);
                }
            }
        })
    })


    $('.submit-ticket').click(function () {

        var status = $(this).data('status');
        $('#status').val(status);

        $.easyAjax({
            url: '<?php echo e(route('admin.tickets.store')); ?>',
            container: '#storeTicket',
            type: "POST",
            data: $('#storeTicket').serialize()
        })
    });

    $('#add-type').click(function () {
        var url = '<?php echo e(route("admin.ticketTypes.createModal")); ?>';
        $('#modelHeading').html("<?php echo e(__('app.addNew').' '.__('modules.tickets.ticketTypes')); ?>");
        $.ajaxModal('#ticketModal', url);
    })

    $('#add-channel').click(function () {
        var url = '<?php echo e(route("admin.ticketChannels.createModal")); ?>';
        $('#modelHeading').html("<?php echo e(__('app.addNew').' '.__('modules.tickets.ticketTypes')); ?>");
        $.ajaxModal('#ticketModal', url);
    })

    function setValueInForm(id, data){
        $('#'+id).html(data);
        $('#'+id).selectpicker('refresh');
    }
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>