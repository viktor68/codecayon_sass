<?php $__empty_1 = true; $__currentLoopData = $employeeDocs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$employeeDoc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <tr>
        <td><?php echo e($key+1); ?></td>
        <td><?php echo e(ucwords($employeeDoc->name)); ?></td>
        <td>
            <a href="<?php echo e(route('member.employee-docs.download', $employeeDoc->id)); ?>"
               data-toggle="tooltip" data-original-title="Download"
               class="btn btn-inverse btn-circle"><i
                        class="fa fa-download"></i></a>
            <a target="_blank" href="<?php echo e(asset('user-uploads/employee-docs/'.$employeeDoc->user_id.'/'.$employeeDoc->hashname)); ?>"
               data-toggle="tooltip" data-original-title="View"
               class="btn btn-info btn-circle"><i
                        class="fa fa-search"></i></a>
            <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" data-file-id="<?php echo e($employeeDoc->id); ?>"
               data-pk="list" class="btn btn-danger btn-circle sa-params"><i class="fa fa-times"></i></a>
        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <tr>
        <td colspan="3"><?php echo app('translator')->getFromJson('messages.noDocsFound'); ?></td>
    </tr>
<?php endif; ?>