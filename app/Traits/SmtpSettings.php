<?php
/**
 * Created by PhpStorm.
 * User: DEXTER
 * Date: 24/05/17
 * Time: 11:29 PM
 */

namespace App\Traits;

use App\GlobalSetting;
use App\PushNotificationSetting;
use App\SmtpSetting;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\Facades\Config;

trait SmtpSettings{

    public function setMailConfigs(){
        $smtpSetting = SmtpSetting::first();
        $pushSetting = PushNotificationSetting::first();
        $company = company();
        $settings = GlobalSetting::first();
        $companyName = $smtpSetting->mail_from_name;

        if($company){
            $companyName = $company->company_name;
        }

        Config::set('mail.driver', 'smtp');
        Config::set('mail.host', 'smtp.gmail.com');
        Config::set('mail.port', '587');
        Config::set('mail.username', 'ttco201909@gmail.com');
        Config::set('mail.password', 'anhtuan123@A');
        Config::set('mail.encryption', 'tls');
        Config::set('mail.from.name', 'TTCO');
        Config::set('mail.from.address', 'ttco@tuoitre.com.vn');

        Config::set('services.onesignal.app_id', $pushSetting->onesignal_app_id);
        Config::set('services.onesignal.rest_api_key', $pushSetting->onesignal_rest_api_key);

        Config::set('app.name', 'TTCO');

        if(($company) && !is_null($company->logo)) {
            Config::set('app.logo', asset('user-uploads/app-logo/'.$company->logo));

        }elseif(!is_null($settings->logo)){
            Config::set('app.logo', asset('user-uploads/app-logo/'.$settings->logo));
        }
        else{
            Config::set('app.logo', asset('worksuite-logo.png'));
        }

        (new MailServiceProvider(app()))->register();
    }

}